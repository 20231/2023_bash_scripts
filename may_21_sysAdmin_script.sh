#!/bin/bash

source colour_coding.sh

echo ""
echo ""
echo "${txtylw}${txtbld}==================================================${txtrst}"
echo "${txtylw}${txtbld}======                                       =====${txtrst}"
echo "${txtylw}${txtbld}======     System Administration Program     =====${txtrst}"
echo "${txtylw}${txtbld}======                                       =====${txtrst}"
echo "${txtylw}${txtbld}==================================================${txtrst}"
echo ""
echo ""

# System administration script.

# Function to display the menu.
display_menu() {
    echo "${txtylw}${txtbld}System Administration Menu${txtrst}"
    echo "${txtylw}${txtbld}--------------------------${txtrst}"
    echo "${txtylw}${txtbld}1. Check disk usage${txtrst}"
    echo "${txtylw}${txtbld}2. Check memory usage${txtrst}"
    echo "${txtylw}${txtbld}3. Show logged in users${txtrst}"
    echo "${txtylw}${txtbld}4. Restart Apache${txtrst}"
    echo "${txtylw}${txtbld}5. Exit${txtrst}"
    echo 

}



# Function to check disk usasge.
check_disk_usage() {
    df -h
}


# Function to check memory usage.
check_memory_usage() {
    free -m
}



# Function to show logged in users.
show_logged_in_users() {
    who

}



# Function to restart Apache.
restart_apache() {
    systemctl restart apache2

}


# Main script logic.
while true; do 
    display_menu
    read -p "${txtylw}${txtbld}Enter your choice:${txtrst} " choice
    echo 

    case $choice in 
        1)
            check_disk_usage
            ;;
        2)
            check_memory_usage
            ;;
        3)
            show_logged_in_users
            ;;
        4)
            restart_apache
            ;;
        5)
            echo "${txtylw}${txtbld}Exiting...${txtrst}"
            break
            ;;
        *)
            echo "${txtylw}${txtbld}Invalid choice. Please try again.${txtrst}"
            ;;
    esac



    echo
done



