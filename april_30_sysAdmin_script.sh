#!/bin/bash

source colour_coding.sh


echo ""
echo ""
echo "${txtylw}${txtbld}==============================================${txtrst}"
echo "${txtylw}${txtbld}=====                                    =====${txtrst}"
echo "${txtylw}${txtbld}=====     System Administration Menu     =====${txtrst}"
echo "${txtylw}${txtbld}=====                                    =====${txtrst}"
echo "${txtylw}${txtbld}==============================================${txtrst}"
echo ""
echo ""

# Display IP address.
echo "${txtylw}${txtbld}The IP address for this machine is: ${txtrst}"
echo "${txtylw}${txtbld}====================================${txtrst}"
ifconfig  eth0 | grep -oP 'inet\s+\K[\d.]+'

echo ""
echo ""

# Display the current date and time.
echo "${txtylw}${txtbld}The current date and time is: ${txtrst} $(date) ${txtrst}"
echo ""

# Display the system uptime.
echo "${txtylw}${txtbld}====================================={$txtrst"
echo ""
echo "${txtylw}${txtbld}The system has been ${txtrst}$(uptime -p)."

# Display the system load average.
echo "${txtylw}${txtbld}The current system load average is ${txtrst}$(uptime | awk '{print $8, $9, $10}')."

# Display the number of logged in users.
echo "${txtylw}${txtbld}There are currently ${txtrst} $(who | wc -l) ${txtylw}${txtbld}user(s) logged in ${txtrst}"

# Display the amount of free memory.
echo "${txtylw}${txtbld}There is currently ${txtrst} $(free -h | awk 'NR==2{print $4}') ${txtylw}${txtbld} of free memory ${txtrst}."

# Display the amount of disk space used and available for the root file system.
echo "${txtylw}${txtbld}The root file system is currently using ${txtrst} $(df -h / | awk 'NR==2 {print $3}') ${txtylow}${txtbld}"

echo "${txtylw}${txtbld}The root file system has ${txtrst} $(df -h / | awk 'NR==2{print $4}') ${txtylw}${txtbld}of available disk space ${txtrst}."

# Check the /etc/shadow file or unauthorised log ins."
echo ""
echo ""
echo "${txtylw}${txtbld}Last 10 lines of /etc/shadow file.${txtrst}"
echo "${txtylw}${txtbld}==================================${txtrst}"
sudo cat /etc/shadow | tail -n 10
echo ""
echo ""

# Display the running processes on a system.
# This information may be used to diagnose performance issues.
ps | awk 'NR==1{printf "\033[1;33m%s\t%s\033[0m\n", $1, $4} NR>1{print $1 "\t" $4}'
echo ""
echo ""
echo ""
